# Custom exceptions for the most common errors in the simulation.
# writing to a full FIFO and writing an unsorted event in the final FIFO 

class FIFOFullError(IndexError):
    '''Raise when trying to write in a full FIFO'''

class UnsortedError(ValueError):
    '''Raise when writing an unsorted event in a FIFOSorted'''