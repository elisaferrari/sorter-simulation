from fifo import FIFO
from eventgenerator import EventGenerator
import math
import sys

class Detector:
    def __init__(self,id,channels,dead_time,fifo_size,clock_frequency,rate,p_channels,n_channels):
        
        # Setting clock information
        self.ClockFrequency = clock_frequency
        self.ClockPeriod = 1e9 / (clock_frequency  * 1e6)
        self.ClockCycles = 0
        
        # Setting detector physical parameters
        self.Channels = channels
        self.DeadTime = dead_time
        self.FifoSize = fifo_size

        # Setting properties to run the simulation
        self.ID = id
        self.BusyFor = 0
        self.EventUnderProcess = None

        # Initializing FIFOs
        self.FIFO_RawEvents = FIFO( maxlen = sys.maxsize, label = 'FIFO_RawEvents Detector {}'.format(self.ID)) # this FIFO is infinite since it's not real, it's just a buffer for the events
        self.FIFO_ProcessedEvents = FIFO( maxlen = fifo_size, label = 'FIFO_ProcessedEvents Detector {}'.format(self.ID) )

        # Initializing event generator
        self.EventGenerator = EventGenerator(rate = rate, p_channels = p_channels, n_channels = n_channels)
        
        # Batch size of event generation, this number should be fine for most situations 
        # except a very fast event rate combined with a very large dead time (which do not make much sense)
        self.DefaultEventNumber = 10000

        
    # Define repr and str overloads for printing and debugging
    def __repr__(self):
        s = ''
        s += 'Detector information:'
        s += '\n\tFifo size: {}\n\tClock frequency: {} MHz\n\tClock period: {} ns\n\tChannels: {}\n\tDead time: {} ns'.format(
            self.FifoSize, self.ClockFrequency, self.ClockPeriod, self.Channels, self.DeadTime
        )
        s += '\nDetector state:'
        s += '\n\tTime elapsed: {}\n\tBusy for {}\n\tEvent under process: {}\n\tProcessed events: {}'.format(
            self.ClockCycles * self.ClockPeriod, self.BusyFor,self.EventUnderProcess,self.FIFO_ProcessedEvents) 
        return s
    
    __str__ = __repr__


    # Function to call each clock cycle. This should perform all the actions of the component
    def CycleClock(self):

        self.ClockCycles += 1

        # If the rawevent fifo is empty we refill it generating new events
        if self.FIFO_RawEvents.Length == 0:
            self.RefillFIFO_RawEvents(self.DefaultEventNumber)

        # If busy do nothing.
        if self.BusyFor > 1 :
            self.BusyFor -= 1

        # If this is the last cycle we are busy it means that we
        # have finished processing the event and we can write it into FIFO_ProcessedEvents
        elif self.BusyFor == 1:
            self.BusyFor = 0
            self.FIFO_ProcessedEvents.Enqueue(self.EventUnderProcess)
            self.EventUnderProcess = None

        # If we are not busy we check if there is an event to process    
        else:
            self.CheckEventQueue()

    
    # Helper function for CycleClock: check if there is an event to process
    def CheckEventQueue(self):
        mintime = self.ClockPeriod * (self.ClockCycles - 1)
        currtime = mintime + self.ClockPeriod

        # These events have been lost since the detector was busy when they arrived, they must be deleted
        while(self.FIFO_RawEvents.Length > 0 and self.FIFO_RawEvents.Peek().Timestamp < mintime):
            self.FIFO_RawEvents.Pop()
        
        # If there is an event with the right timestamp, take the event out and process it
        try:
            t = self.FIFO_RawEvents.Peek().Timestamp
        except IndexError:
            # The queue is empty, nothing bad, we return and don't Pop().
            return
        if ( currtime > t ):
            self.GenerateEvent(self.FIFO_RawEvents.Pop())

    
    # Helper function for CheckEventQueue: initiates processing of an incoming event
    def GenerateEvent(self,Event):
        
        # If we got here the detector is not busy and we can process the event
        self.EventUnderProcess = Event
            
        # Set number of clock cycles the detector will be busy in order to process the event, integer division rounded up
        self.BusyFor = math.ceil( (Event.InvolvedChannels * self.DeadTime) / self.ClockPeriod ) + 1


    # Helper function to fill FIFO_RawEvents when they are exausted
    def RefillFIFO_RawEvents(self, number):
        self.EventGenerator.FillFIFO( fifo = self.FIFO_RawEvents, number = number, id = self.ID)
        