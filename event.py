class Event:
    def __init__(self,timestamp,detector_id,involved_channels):

        # Setting event parameters. Timestamps unit depends on how they have been generated.
        # If using the event generator class with a rate in evt/ns, timestamps will be in ns
        # InvolvedChannels counts the number of channels activated by the event
        self.Timestamp = timestamp
        self.DetectorID = detector_id
        self.InvolvedChannels = involved_channels

        # Comparison counter for the sorter stages
        self.Comparisons = 0
    
    
    # Define repr and str overloads for printing and debugging
    def __repr__ (self):
        return '[TS: {:.3f}, ID: {}, CH: {}, CP: {}]'.format(self.Timestamp, self.DetectorID, self.InvolvedChannels, self.Comparisons)
    
    __str__ = __repr__