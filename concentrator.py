from fifo import FIFO

class Concentrator:
    def __init__(self, clock_frequency, fifos_in, fifo_out):
        
        # Setting clock information
        self.ClockFrequency = clock_frequency
        self.ClockPeriod = 1e9 / (clock_frequency  * 1e6)
        self.ClockCycles = 0
        
        # Initializing FIFOs
        self.FIFO_Out = fifo_out
        self.FIFOs_In = fifos_in
        self.FIFO_Number = len(fifos_in)

        # Set Concentrator index, needed to cycle through the FIFOs
        self.Index = 0


    # Function to call each clock cycle. This should perform all the actions of the component
    def CycleClock(self):
        self.ClockCycles += 1

        if (not self.FIFOs_In[self.Index].IsEmpty()) and (not self.FIFO_Out.IsFull()) :
            self.FIFO_Out.Enqueue( self.FIFOs_In[self.Index].Pop() )
        self.Index = (self.Index + 1) % self.FIFO_Number


# This is needed by the sorter stages, since they need to prioritize FIFO_Fail. 
# Maybe it should be implemented as a concentrator child class.
class PriorityConcentrator:
    def __init__(self, clock_frequency, fifo_in_1, fifo_in_2, fifo_out):
        
        # Setting clock information
        self.ClockFrequency = clock_frequency
        self.ClockPeriod = 1e9 / (clock_frequency  * 1e6)
        self.ClockCycles = 0
        
        # Initializing FIFOs
        self.FIFO_Out = fifo_out
        self.FIFOs_In = [fifo_in_1, fifo_in_2]
        
        self.FIFO_Number = 2

        # Set Concentrator index, needed to cycle through the FIFOs
        self.Index = 0


    # Function to call each clock cycle. This should perform all the actions of the component
    def CycleClock(self):
        
        self.ClockCycles += 1
        if (not self.FIFOs_In[0].IsEmpty()) :
            self.FIFO_Out.Enqueue( self.FIFOs_In[0].Pop() )
        elif (not self.FIFOs_In[1].IsEmpty()) :
            self.FIFO_Out.Enqueue( self.FIFOs_In[1].Pop() )
