import collections
import sys
from custom_exceptions import FIFOFullError, UnsortedError

# This is a wrapper class for the collections.deque built-in python class. 
# It was needed to throw an error if the FIFO is full and something tries to write in it,
# since in this case the simulation has failed.
class FIFO:
    def __init__(self, maxlen = sys.maxsize, label = ""):

        # Setting class parameters, this class is composed of a deque object and a maxlength value
        self.MaximumLength = maxlen
        self.Queue = collections.deque()
        self.Length = 0
        self.Label = label

    
    # Implement len method
    def __len__(self):
        return self.Length
    

    # Define repr and str methods for printing and debugging
    def __repr__(self):
        return '({}, Current length {}, Maximum length: {})'.format(self.Queue,self.Length,self.MaximumLength)
    
    __str__ = __repr__


    # Wrapper functions that check if the FIFO is full before inserting new elements
    def Enqueue(self,item):
        if self.Length == self.MaximumLength:
            mess = 'Fifo "{}" is full! An event has been lost and thus this sorter simulation has failed. Its maximum length is: {}.'.format(self.Label,self.Length)
            raise FIFOFullError(mess)
        self.Queue.appendleft(item)
        self.Length += 1


    def Extend(self,list):
        if (self.Length + len(list) > self.MaximumLength):
            mess = 'The FIFO cannot be extended so much. Its maximum length is {}'.format(self.MaximumLength)
            raise FIFOFullError(mess)
        self.Queue.extendleft(list)
        self.Length += len(list)
    
    
    # Same functions as deque.pop() and deque.popleft()
    def Pop(self):
        self.Length -= 1
        return self.Queue.pop()

    def PopLeft(self):
        self.Length -= 1
        return self.Queue.popleft()


    # Function to check the first item in the queue without extracting it. Note that this IS possible also in FPGAs
    def Peek(self):
        return self.Queue[-1]


    # Check if queue is empty/full. Needed before popping/inserting   
    def IsEmpty(self):
        return self.Length == 0

    def IsFull(self):
        return self.Length == self.MaximumLength


class FIFOSorted(FIFO):
    
    # Overload needed only for FIFO_Final so that we can stop immediately if an unsorted event is enqueued.
    def Enqueue(self,item):
        if self.Length == self.MaximumLength:
            mess = 'Fifo "{}" is full! An event has been lost and thus this sorter simulation has failed. Its maximum length is: {}.'.format(self.Label,self.Length)
            raise FIFOFullError(mess)
        if self.Length > 0 and item.Timestamp < self.Queue[0].Timestamp:
            mess = 'Error in FIFO "{}". The event inserted in position {} is not sorted, the simulation has failed.'.format(self.Label, self.Length)
            raise UnsortedError(mess)
        self.Queue.appendleft(item)
        self.Length += 1


