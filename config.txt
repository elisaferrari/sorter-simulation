# FIFO sizes are in event numbers, times are in nanoseconds and frequencies are in MHz

[global]
detector_number = 100
detector_event_rate = 0.0001
channel_activation_probability = 0.25

#    Events needed in the final fifo to be sure that the sorter is able to sort the incoming events. 
#    Increasing this value yields a more reliable simulation, but the simulation will also take linearly more time
target_event_number = 10000

#    Max number of stages to check. After this number the simulation will stop. 
#    200 is what a modern medium-grade FPGA can contain and will be fine for most cases
max_stages = 200

#    A maximum threshold shortens the search in the case of very fast sorters and very low-throughput detectors.
#    A threshold higher than 20 is unlikely to help the sorter much more.
max_threshold = 20

[concentrator]
clock_frequency = 100
fifo_size = 100

[detector]
clock_frequency = 100
fifo_size = 100
channels = 64
dead_time = 3

[sorter_stage]
clock_frequency = 200
fifo_size = 100
efficiency = 3


