#!/usr/bin/env python

import os, sys
import collections
import constants
from fpga import FPGA
from simulation import Simulation
from sorter_configuration import SorterConfiguration
this_path, this_file = os.path.split(os.path.abspath(__file__))



def main(args):

    # Set verbosity based on command line arguments.
    if args.verbose :
        constants.C_VERBOSITY = 1
    if args.very_verbose :
        constants.C_VERBOSITY = 2


    # Read configuration.
    print ('Reading configuration ...')
    Configuration = SorterConfiguration(args.configuration)
    print ('Configuration file is correct. Initializing simulation...')


    # Initialize and run simulation.
    s = Simulation( config = Configuration.ConfigurationDictionary )
    s.RunSimulation()

    # If we got here the simulation succeeded. Print results.
    print('------------------------------------------------------------------------------')
    print('Parameter search has succeeded. The best parameters found are:')
    print('    - Best threshold:     {:4d}'.format(s.BestThreshold))
    print('    - Best stage number:  {:4d}'.format(s.BestStages))
    


if __name__ == '__main__':
    import argcomplete, argparse, traceback
    from argcomplete.completers import ChoicesCompleter
    try:
        parser = argparse.ArgumentParser(prog=this_file)
        parser.add_argument('-c','--configuration', default='config.txt')
        parser.add_argument('-v','--verbose', action = 'store_true')
        parser.add_argument('-vv','--very-verbose', action = 'store_true')
        argcomplete.autocomplete(parser)
        args = parser.parse_args()
        main(args)
        sys.exit(0)
    except KeyboardInterrupt as e:
        raise e
    except SystemExit as e:
        raise e
    except Exception as e:
        traceback.print_exc() # a simple print(e) may be enough, but initially it is useful to see the back trace 
        sys.exit(1)