import time
import timeit
import random
import sys
from detector import Detector
from stage import Stage
from concentrator import Concentrator
from fifo import FIFO, FIFOSorted
from eventgenerator import EventGenerator
from clocker import Clocker
from simulationresult import SimulationResult
from custom_exceptions import FIFOFullError, UnsortedError

class FPGA:
    def __init__(self,configuration_dictionary, stages, threshold):

        # Set parameters
        self.Threshold = threshold
        self.TargetEventNumber = configuration_dictionary['global']['target_event_number']
        
        # Initialize detectors
        self.Detectors = [
            Detector(   
                id = i,
                channels = configuration_dictionary['detector']['channels'],
                dead_time = configuration_dictionary['detector']['dead_time'],
                fifo_size = configuration_dictionary['detector']['fifo_size'],
                clock_frequency = configuration_dictionary['detector']['clock_frequency'],
                rate = configuration_dictionary['global']['detector_event_rate'],
                p_channels = configuration_dictionary['global']['channel_activation_probability'],
                n_channels = configuration_dictionary['detector']['channels']
            ) for i in range (configuration_dictionary['global']['detector_number']) 
        ]

        # Initialize main event FIFO
        self.FIFO_Events = FIFO( maxlen = configuration_dictionary['concentrator']['fifo_size'], label = 'FIFO_Events')
        
        # Initialize concentrator, connecting it to the detectors output FIFOs and to the event fifo
        self.DetectorConcentrator = Concentrator(
            clock_frequency = configuration_dictionary['concentrator']['clock_frequency'],
            fifos_in = [item.FIFO_ProcessedEvents for item in self.Detectors],
            fifo_out = self.FIFO_Events
        )

        # Initialize sorted events FIFO. This should contain the final output 
        # of the simulation (the sorted events). 
        # This FIFO has no maximum length since it is assumed to be emptied
        # regularly by the acquisition system.
        # In the simulation we keep it infinite in order to check if all the events 
        # are really sorted at the end of the acquisition.
        self.FIFO_Final = FIFOSorted( maxlen = sys.maxsize, label = 'FIFO_Final')

        # Initialize sorter stages
        self.Stages = [
            Stage(
                id = j,
                efficiency = configuration_dictionary['sorter_stage']['efficiency'],
                threshold = threshold,
                fifo_size = configuration_dictionary['sorter_stage']['fifo_size'],
                clock_frequency = configuration_dictionary['sorter_stage']['clock_frequency']
            ) for j in range(stages) 
        ]

        # Connect sorter stages

            # First stage gets the FIFO_Events as input
        self.Stages[0].FIFO_Recv = self.FIFO_Events

            # The other stages except the last one are connected each to the following one.
        for i in range(len(self.Stages) - 1):
            self.Stages[i].FIFO_Sorted = self.Stages[i+1].FIFO_Recv
             
            # The last stage output is connected to the final FIFO
        self.Stages[-1].FIFO_Sorted = self.FIFO_Final

            # Reinitialize the internal concentrator to connect to the newly assigned FIFOs 
            # (maybe this should be done with a setter on the FIFOs)
        for stage in self.Stages:
            stage.InitializeConcentrator()


        # Initialize clocker component
        self.Clocker = Clocker( self.Detectors + self.Stages + [self.DetectorConcentrator], 10000 )
        
        return
        
    
    # Check if FIFO_Final contains events and is sorted
    def CheckResult(self):
        if self.FIFO_Final.Length == 0:
            return False
        
        t1 = self.FIFO_Final.Queue[0].Timestamp + 1
        for evt in self.FIFO_Final.Queue:
            if t1 < evt.Timestamp:
                return False
            t1 = evt.Timestamp
        return True

    
    # Run the FPGA to produce a fixed number of events in FIFO_final or stop when an exception is thrown.
    def Run(self):
        s = time.time()

        # Fill detector input FIFOs
        for det in self.Detectors:
            det.DefaultEventNumber = 10000
            det.RefillFIFO_RawEvents(number = det.DefaultEventNumber)

        # Start simulation
        try:
            while(self.FIFO_Final.Length < self.TargetEventNumber):
                self.Clocker.FindNextClockTick()
        
        # Catch fifo full error, exit with errors = true (but don't throw, since this can be normal)
        except FIFOFullError as f:
            e = time.time()
            print(f)
            return SimulationResult( 
                total_events = self.FIFO_Final.Length, 
                sorted_events = self.CheckResult(), 
                fifo_full_errors = True,
                fpga = self,
                time_elapsed = e-s
            )
        # Catch unsorted event error, exit with sorted = false (but don't throw, since this can be normal)
        except UnsortedError as u:
            e = time.time()
            print(u)
            return SimulationResult( 
                total_events = self.FIFO_Final.Length, 
                sorted_events = False, 
                fifo_full_errors = False,
                fpga = self,
                time_elapsed = e-s
            )

        e = time.time()

        # Simulation is over, if we got here the events are sorted
        return SimulationResult( 
            total_events = self.FIFO_Final.Length, 
            sorted_events = True, 
            fifo_full_errors = False,
            fpga = self,
            time_elapsed = e-s
        )

    # This function is identical to Run(), but does not exit when it catches an unsorted error.
    # It is needed when we are looking for the best threshold, which should be stopped only by a FIFO full error.
    def RunForThreshold(self):
        s = time.time()

        # Fill detector input FIFOs
        for det in self.Detectors:
            det.DefaultEventNumber = 10000
            det.RefillFIFO_RawEvents(number = det.DefaultEventNumber)


        sorted_events = True
        # Start simulation
        while(self.FIFO_Final.Length < self.TargetEventNumber):
            try:
                self.Clocker.FindNextClockTick()
        
            # Catch fifo full error, exit with errors = true (but don't throw, since this can be normal)
            except FIFOFullError as f:
                e = time.time()
                print(f)
                return SimulationResult( 
                    total_events = self.FIFO_Final.Length, 
                    sorted_events = sorted_events, 
                    fifo_full_errors = True,
                    fpga = self,
                    time_elapsed = e-s
                )
            # Catch unsorted event error, but don't exit
            except UnsortedError :
                sorted_events = False

        e = time.time()

        # Simulation is over, if we got here the events are sorted
        return SimulationResult( 
            total_events = self.FIFO_Final.Length, 
            sorted_events = sorted_events, 
            fifo_full_errors = False,
            fpga = self,
            time_elapsed = e-s
        )


    # Print various information for debugging and logging
    def DebugInfo(self):
        mess = 'FPGA is running. Detectors: {}. Stages: {}. Threshold: {}. Target event number: {}'.format(
            len(self.Detectors),
            len(self.Stages),
            self.Stages[0].Threshold,
            self.TargetEventNumber
        )
        mess += '\n{}'.format(self.Detectors[0])
        mess += '\n{}'.format(self.Stages[0])
        return mess


    def RunTests(self):
        ###################################################
        # Tests


        ####################################
        # Everything OK!
        m = 0
        m1 = 0
        s = time.time()
        k = 0
        while(self.FIFO_Final.Length < 10000 ):
            k+=1
            for det in self.Detectors:
                det.CycleClock()
            for stage in self.Stages:
                stage.CycleClock()
            for stage in self.Stages:
                stage.CycleClock()
            for stage in self.Stages:
                stage.CycleClock()
            for stage in self.Stages:
                stage.CycleClock()
            self.DetectorConcentrator.CycleClock()
            self.DetectorConcentrator.CycleClock()
            m = max(self.FIFO_Events.Length, m)
            m1 = max(self.Detectors[0].FIFO_ProcessedEvents.Length, m1)
        e = time.time()
        print('Elapsed: {}. Cycles {}'.format((e-s), k) )
        print('Events outputted: {}'.format(self.FIFO_Final.Length))
        print('Is sorted?: {}'.format(self.CheckResult()))
        print('Max FIFO_Events length: {}'.format(m))
        print('Max Detector FIFO length: {}'.format(m1))
        #################################################################
        # Multiple sorters are OK!
        #self.Detectors[0].EventGenerator.LastEvent = None
        #size = 100
        #li = self.Detectors[0].EventGenerator.Generate(size,0)
        #list_shuf = random.sample(li,len(li))
        #self.FIFO_Events.Extend(list_shuf)
        #k=0
        #while (self.FIFO_Final.Length < 5*size):
        #    if self.FIFO_Events.Length == 0:
        #        li = self.Detectors[0].EventGenerator.Generate(size,0)
        #        list_shuf = random.sample(li,len(li))  
        #        self.FIFO_Events.Extend(list_shuf)
        #    k+=1
        #    for stage in self.Stages :
        #        stage.CycleClock()
        #print('cycles {}'.format(k))
        #print('events outputted: {}'.format(self.FIFO_Final.Length))
        #print('Is sorted?: {}'.format(self.CheckResult()))
        
        
            
        #################################################################
        # Single stage OK! Works correctly
        #self.Detectors[0].EventGenerator.LastEvent = None
        #li = self.Detectors[0].EventGenerator.Generate(1000,0)
    
        #list_shuf = random.sample(li,len(li))
        #
        #self.FIFO_Events.Extend(list_shuf)
        #item = self.Stages[0]
        #item2 = self.Stages[1]
        #for i in range(30000):
        #    item.CycleClock()
        #    item2.CycleClock()
    
        #print(item)
        #print(item.FIFO_Sorted.Length)
        #print(self.Stages[1].FIFO_Recv.Length)
        #print(item2)
        #print(item2.FIFO_Sorted.Length)
        #print(self.Stages[2].FIFO_Recv.Length)
        #################################################################
        # Multiple detectors OK!
        #cycles = 10000
        #s = time.time()
        #for i in range(cycles):
        #    for det in self.Detectors:
        #        det.CycleClock()
        #    self.DetectorConcentrator.CycleClock()
        #print(self.FIFO_Events.Length)
        
        #for item in self.Detectors:
        #    print(item.FIFO_ProcessedEvents.Length)
        ##for i in self.FIFO_Events.Queue:
        ##    print('ID: {}   TS: {}'.format(i.DetectorID,i.Timestamp))
        #e = time.time()
        #print('Elapsed: {}'.format(e-s))
            
        #################################################################
        ## Single detector OK! 2.5 s for 1.000.000 cycles
        #item = self.Detectors[0]
        #cycles = 1000
        #s = time.time()
        #for i in range(cycles):
        #    #if item.FIFO_RawEvents.IsEmpty():
        #    #    cycles = i
        #    #    print('Exiting after {} clocks'.format(i))
        #    #    break
        #    item.CycleClock()
        #e = time.time()
        #print('Elapsed: {}. Cycles: {}. Time per cycle: {:.3e} s'.format( (e-s), cycles, (e-s)/cycles   ))
        #print(len(item.FIFO_ProcessedEvents))
        #print(len(item.FIFO_RawEvents))
        #print(item.FIFO_ProcessedEvents)
        #for i in range(20):
        #    item.CycleClock()
        #    print(item)
        #self.Stages[0].FIFO_Sorted.Enqueue(1)
        #self.Stages[0].FIFO_Sorted.Enqueue(2)
        #self.Stages[1].FIFO_Recv.Pop()
        #self.Stages[-1].FIFO_Sorted.Enqueue('a')
        #for i in range(len(self.Stages)):
        #    print('{}th stage.\n\tFIFO_Recv: {}\n\tFIFO_Sorted: {}'.format(i,self.Stages[i].FIFO_Recv,self.Stages[i].FIFO_Sorted))
        #print('FIFO final: ' + str(self.FIFO_Final))