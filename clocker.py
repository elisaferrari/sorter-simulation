from fifo import FIFO
from clockgroup import ClockGroup
from numpy import where, array
from collections import OrderedDict

# This component will determine what FPGA component to clock next. Note that multiple components may clock at the same time
# If this happens, the components scheduled for clocking are clocked sequentially, in the order in which they have been inserted
# in the clocker.
# Since clock domains are always separated by FIFOs (even in hardware), the Concentrators are those that should clock last in
# the simulation in order to give the most realistic effect possible.
class Clocker:
    def __init__(self,components,batch_size):

        # Initialize parameters and components
        self.BatchSize = batch_size # how many clock at a time should be loaded into the clock FIFOs
        self.Components = None

        # Prepare FIFOs with clock times for every clock group
        self.ClockTicks = None

        # Build clock groups unifying the components with the same clock period
        self.BuildClockGroups(components)


    # Build clock groups with components sharing the same clock
    def BuildClockGroups(self,components):
        
        # Get clock information
        clock_periods = [comp.ClockPeriod for comp in components ]
        clock_set = list(OrderedDict.fromkeys(clock_periods)) # or also: set( clock_periods )

        # Build clock groups
        self.ClockGroups = []
        for el in clock_set:
            cp = array( clock_periods )
            ind = where( cp == el)[0]
            self.ClockGroups.append(ClockGroup(
                components = [ components[i] for i in ind ], 
                batchsize = self.BatchSize
                ) 
            )

        # Set clocker properties to bind to the clock groups
        self.ClockTicks = [ group.ClockTicks for group in self.ClockGroups ]
        self.ClockPeriods = [ group.ClockPeriod for group in self.ClockGroups ]
        self.Components = self.ClockGroups



    # Function that clocks the components in the correct order.
    def FindNextClockTick(self):

        # Find the next component groups that should clock
        nextclocks = array([fifo.Queue[-1] for fifo in self.ClockTicks])
        min_val = min(nextclocks)
        indexes = where(nextclocks == min_val)[0]

        # Make them clock
        for index in indexes:
            self.Components[index].CycleClock()