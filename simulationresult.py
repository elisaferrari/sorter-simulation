class SimulationResult:
    def __init__(self,total_events,sorted_events,fifo_full_errors,fpga,time_elapsed = -1):
        
        # Set simulation parameters
        self.Detectors = len(fpga.Detectors)
        self.Stages = len(fpga.Stages)   
        self.D0 = fpga.Detectors[0]
        self.S0 = fpga.Stages[0]
        self.Threshold = fpga.Threshold
        

        # Set outcome of the simulation
        self.TotalEvents = total_events
        self.Sorted = sorted_events
        self.FIFOFullErrors = fifo_full_errors
        self.TimeElapsed = time_elapsed


    # Provide repr and str methods for printing
    def __repr__(self):
        s = ''
        s+= 'Run ended. Detectors #: {}. Stages #: {}. Threshold: {}\n'.format(self.Detectors,self.Stages,self.Threshold)
        if self.TimeElapsed != -1:
            s += 'Time elapsed {:.3f} s. '.format(self.TimeElapsed)
        
        s += 'Total events in the final FIFO: {}.\nSorted: {}. Errors: {}.'.format(self.TotalEvents,self.Sorted,self.FIFOFullErrors)
        
        if self.Sorted and (not self.FIFOFullErrors):
            return s + ' Run has succeded'
        else:
            return s + ' Run has failed'