from fpga import FPGA
from simulationresult import SimulationResult
from constants import logger
from numpy import clip
from custom_exceptions import FIFOFullError, UnsortedError

class Simulation:
    def __init__(self, config):

        # Setting parameters
        self.Configuration = config

        self.MaxStages = self.Configuration['global']['max_stages']
        self.MaxThreshold = self.Configuration['global']['max_threshold']
        
        self.FPGA = None
        self.BestThreshold = 0
        self.BestStages = 0


    # Functions that performs the search
    def RunSimulation(self):
        
        logger('Starting research for best threshold...',0)
        self.SearchThreshold(stages = 5)
        logger('Best threshold found: {}'.format(self.BestThreshold),0)
        
        #self.BestThreshold = 2
        
        while( True ):
            try:
                logger('Starting research for best stage number. Using threshold {}'.format(self.BestThreshold),0)
                
                self.SearchStageNumber()
                logger('Best stage number found: {}'.format(self.BestStages),0)
                return

            except ValueError as v:
                if self.BestThreshold >= 2:
                    logger('Best stage number could not be found. Retry with lower threshold')
                    self.BestThreshold -= 1
                else :
                    raise v

    # Helper functions that searches for the best threshold
    def SearchThreshold(self,stages):

        # Set best threshold to 0. We will check if this has been modified to see if the simulation gave results
        self.BestThreshold = 0

        # Define starting point and step size
        thr = clip(self.MaxThreshold // 10, 1, self.MaxThreshold)
        step = clip(self.MaxThreshold // 10, 1, self.MaxThreshold)
        max_thr = self.MaxThreshold
        
        while (thr <= max_thr):
            if thr == max_thr and thr != self.MaxThreshold:
                if step != 1:
                    thr = thr - step + 1
                    step = clip(step // 10,1, self.MaxThreshold//10 )
                else:
                     break

            # Initialize the FPGA
            self.FPGA = FPGA( configuration_dictionary = self.Configuration, stages = stages, threshold = thr)

            # Log some information
            logger(self.FPGA.DebugInfo(), priority = 2)
            logger('Starting run for best threshold. Stage #: {}. Threshold: {}'.format(len(self.FPGA.Stages),self.FPGA.Threshold), 1)
            
            res = self.FPGA.RunForThreshold()

            logger(res,1)

            if res.FIFOFullErrors:
                
                # It does not make sense to go higher than this (since this thr produced an error):
                max_thr = thr
                
                # If there has been an error we should lower the step size and restart from the last good value
                if step > 1:
                    thr -= step
                    newstep = clip(step // 10,1, self.MaxThreshold // 10 )
                    step = newstep

                # If we can't lower the step size than we have found what we were looking for
            else:
                # Our best guess for BestThreshold is this:
                self.BestThreshold = thr

            thr += step

        # The most stable threshold is one below the highest possible one
        if self.BestThreshold >= 2:
            self.BestThreshold -= 1

        
        # Throw error if the search space has been exausted
        if self.BestThreshold == 0:
            raise ValueError('''The simulation could not handle all the events even with a threshold of 1.
            The parameters you have entered are too taxing on the sorter. Do one or more of the following:
                - Raise the sorter stage clock frequency
                - Lower the sorter efficiency
                - Raise the concentrator clock frequency
                - Lower the number of detectors or their event rate
                ''')
        

    # Helper function that searches for best stage number
    def SearchStageNumber(self):

        # Define starting point and step size
        stages = clip( self.MaxStages // 10, 1, self.MaxStages )
        step = clip( self.MaxStages // 10, 1, self.MaxStages )
        max_stages = self.MaxStages
        self.BestStages = 0
        
        while ( stages <= max_stages ):
            if stages == max_stages and stages != self.MaxStages:
                if step != 1:
                    stages = stages - step + 1
                    step = clip(step // 10,1, self.MaxStages//10 )
                else:
                     break


            # Initialize FPGA
            self.FPGA = FPGA( configuration_dictionary = self.Configuration, stages = stages, threshold = self.BestThreshold)
 
            # Log some information
            logger (self.FPGA.DebugInfo(),2)
            logger('Starting run for best stage number. Stage #: {}. Threshold: {}'.format(len(self.FPGA.Stages),self.FPGA.Threshold),1)
            
            # Run the simulation
            res = self.FPGA.Run()

            logger(res,1)
        
            if res.Sorted:

                # We don't need to look higher than this, since we already know this is fine
                max_stages = stages
                self.BestStages = stages

                # If we had no error we should lower the step size and restart from the last bad value
                if step > 1:
                    stages -= step
                    newstep = clip(step // 10,1, self.MaxStages//10 )
                    step = newstep
                
                # If we can't lower the step size than we have found what we were looking for

            stages += step

        if self.BestStages == 0 :
            raise ValueError('''The simulation could not find a suitable number of stages.
            The parameters you have entered are too taxing on the sorter. Do one or more of the following:
                - Raise the maximum stage limit in the configuration file (note that this just tries to add more stages, 
                  it is not guaranteed to find a solution and will take a long time)
                - Lower the number of detectors or their event rate
                - Increase sorter clock frequency. This will hopefully yield a higher threshold that will make the sorter task easier.
                ''')