from fifo import FIFO
from concentrator import PriorityConcentrator
from event import Event

class Stage:
    def __init__(self,id,fifo_size,clock_frequency,threshold,efficiency):
        
        # Setting clock information
        self.ClockFrequency = clock_frequency
        self.ClockPeriod = 1e9 / (clock_frequency  * 1e6)
        self.ClockCycles = 0
        
        # Setting sorter parameters
        self.ID = id
        self.FifoSize = fifo_size
        self.BusyFor = 0
        self.Threshold = threshold
        self.Efficiency = efficiency

        # Initializing FIFOs
        self.FIFO_Recv = FIFO(maxlen = fifo_size, label = 'FIFO_Recv Stage {}'.format(self.ID))
        self.FIFO_Fail = FIFO(maxlen = fifo_size, label = 'FIFO_Fail Stage {}'.format(self.ID))
        self.FIFO_Curr = FIFO(maxlen = 2, label = 'FIFO_Curr Stage {}'.format(self.ID)) # This storage unit could be something else than a FIFO, but it's fine like this.
        self.FIFO_Sorted = None # this will be connected to the FIFO_Recv of the next stage.

        # Initializing internal concentrator that puts together FIFO_Recv and FIFO_Fail into FIFO_Curr
        self.Concentrator = PriorityConcentrator( 
            clock_frequency = clock_frequency, 
            fifo_in_1 = self.FIFO_Fail,
            fifo_in_2 = self.FIFO_Recv, 
            fifo_out = self.FIFO_Curr
        )

        # Container for the event to be written in FIFO_Fail
        self.EventToBeWritten = None
        

    # Define repr and str overloads for printing and debugging
    def __repr__(self):
        s = ''
        s += 'Stage information:'
        s += '\n\tFifo size: {}\n\tClock frequency: {} MHz\n\tEfficiency: {} cycles\n\tThreshold: {} cycles'.format(
            self.FifoSize, self.ClockFrequency, self.Efficiency, self.Threshold
        )
        s += '\nStage state:'
        s += '\n\tFifo_recv length: {}\n\tFifo_fail length: {}\n\tFifo_curr: {}\n\tEvent to be written: {}.\n\tBusy for: {} cycles'.format(
            self.FIFO_Recv.Length, self.FIFO_Fail.Length, self.FIFO_Curr, self.EventToBeWritten, self.BusyFor
        )
        return s
    
    __str__ = __repr__

    
    # Function to call each clock cycle. This should perform all the actions of the component
    def CycleClock(self):
        
        self.ClockCycles += 1

        # See if we can Cycle() the concentrator to push an event in FIFO_Curr
        if self.FIFO_Curr.Length < self.FIFO_Curr.MaximumLength:
            self.Concentrator.CycleClock()

        # If busy do nothing
        if self.BusyFor > 0 :
            self.BusyFor -= 1
        else :
            if self.FIFO_Curr.Length > 0 :
                self.CheckSortedEvent()

            # Check if there is an event to be written in FIFO_fail
            if self.EventToBeWritten != None and (not self.FIFO_Fail.IsFull()):
                self.FIFO_Fail.Enqueue(self.EventToBeWritten)
                self.EventToBeWritten = None

            # If FIFO_Curr is full (2 events) we can compare the events
            if self.FIFO_Curr.Length == self.FIFO_Curr.MaximumLength and self.EventToBeWritten == None:
                self.CompareEvents(self.FIFO_Curr)
            


    # Helper function for CycleClock: it checks if an event has been compared enough times
    def CheckSortedEvent(self):
        # We need to Peek() because we do not want to take
        # the event out of the FIFO if it is not ready
        if self.FIFO_Curr.Peek().Comparisons >= self.Threshold and (not self.FIFO_Sorted.IsFull()):
            e = self.FIFO_Curr.Pop()
            e.Comparisons = 0
            self.FIFO_Sorted.Enqueue(e)


    # Helper function for CycleClock: it compares the two events in FIFO_Curr
    def CompareEvents(self, fifo):
        
        # Give easier names to the events
        e_left = fifo.Queue[0]
        e_right = fifo.Queue[1]

        # See which event has to go through
        if (e_left.Timestamp <= e_right.Timestamp) :
            e_left.Comparisons += 1
            self.EventToBeWritten = fifo.Pop()
        else:
            e_right.Comparisons += 1
            self.EventToBeWritten = fifo.PopLeft()
        
        # Set the busy period
        self.BusyFor = self.Efficiency
    
    def InitializeConcentrator(self):
        self.Concentrator = PriorityConcentrator( 
            clock_frequency = self.ClockFrequency, 
            fifo_in_1 = self.FIFO_Fail,
            fifo_in_2 = self.FIFO_Recv, 
            fifo_out = self.FIFO_Curr
        )