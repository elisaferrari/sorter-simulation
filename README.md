# Sorter simulation

This git repository contains the code for the simulation of an event sorter hardware component for Positron Emission Tomography applications.
In a PET scan, the detectors produce event packets that are gathered by the acquisition system, typically implemented in FPGAs. While the events are always assigned a correct timestamp, sometimes their order in the acquisition stream may be incorrect.
Since only the events arrived in couples need to be acquired to produce a PET image, it is necessary to select them from the event stream and discard the others. In order to easily do this with FPGAs, the events must be sorted by their timestamp.
This program will simulate a staged sorter component in order to understand the correct parameters needed in order to sort an event stream.

The code is divided in different files, each .py file aside from "main_sorter.py" (the actual executable script of the program) implements a class and, if present, its child classes. The "config.txt" file contains the configuration parameters that the user needs to supply to run the program.
Note that the program will run with any physically possible parameter in the configuration files, but for certain combination of parameters the sorter may not be able to sort the event stream or the simulation may take an incredibly long time to complete.

------

# Dependencies

This program has been tested with Python 3.7 on Windows 10 and Python 3.4 on RedHat 7.5. The following python libraries are needed:

1. Numpy
2. Argcomplete and Argparse (to read command line arguments, not fundamental for running the program)

------

# How to run

Run with config.txt file as configuration file and with almost no verbosity (only final output displayed).  
$] python main_sorter.py

**Increase verbosity:**  
$] python main_sorter.py -v  
$] python main_sorter.py -vv

**Pass custom configuration file:**  
$] python main_sorter.py -c '/path/to/file' \[-v\]