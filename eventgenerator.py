from event import Event
import numpy

class EventGenerator:

    # Initialize event generator with event rate seen by the detector, 
    # probability of activating a channel in the detector and
    # total number of channels in the detector.
    def __init__(self, rate, p_channels, n_channels ):
        
        # Probability that an event triggers one channel
        self.ChannelProbability = p_channels 
        self.ChannelNumber = n_channels
        
        # Rate is assumed to be in events/ns
        self.Rate = rate

        # Last event kept in memory in order to generate new ones
        self.LastEvent = None
    

    # Function to actually generate the events.
    # Note that the number of activated channels is random and follows a binomial distribution,
    # while the time that occurs between an event and the following one follows an exponential distribution.
    def Generate(self, number, id):

        # Binomial distribution to get the number of activated channels
        ch = numpy.random.binomial(n = self.ChannelNumber, p = self.ChannelProbability, size = number)

        # numpy.cumsum() to get the timestamps instead of the time intervals
        # Since rate is in evt/ns, timestamps will be in ns
        if self.LastEvent == None:
            ofs = 0
        else :
            ofs = self.LastEvent.Timestamp
        
        ts = ofs + numpy.cumsum(numpy.random.exponential(scale = 1.0 / self.Rate, size = number))
        
        # Build event object
        evt_list = [Event(timestamp = ts[i], detector_id = id, involved_channels = ch[i]) for i in range(number)]
        self.LastEvent = evt_list[-1]

        return evt_list


    # Puts the generated events into a FIFO
    def FillFIFO(self, fifo, number, id):
        events = self.Generate(number, id)
        fifo.Extend(events)