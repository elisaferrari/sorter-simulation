from fifo import FIFO
from numpy import arange

class ClockGroup:
    def __init__(self,components,batchsize):
        
        # Check if the clock periods are all the same
        assert len(set([comp.ClockPeriod for comp in components])) == 1
        
        # Set properties
        self.Components = components
        self.ClockPeriod = self.Components[0].ClockPeriod
        self.BatchSize = batchsize
        self.ClockTicks = FIFO(self.BatchSize)

        # Fill the FIFOs with the correct times
        self.ClockTicks.Extend(
            arange(
                self.ClockPeriod,
                self.BatchSize * self.ClockPeriod,
                self.ClockPeriod
            )
        )
        

    # Function that instructs the components of the group to do a clock cycle
    def CycleClock(self):

        # Make the components clock
        for comp in self.Components:
            comp.CycleClock()
        
        # Pop() the ticks FIFO and refill it if necessary
        tick = self.ClockTicks.Pop()
        if self.ClockTicks.Length == 0:
            self.ClockTicks.Extend(
                arange(
                    tick + self.ClockPeriod,
                    tick + self.BatchSize * self.ClockPeriod,
                    self.ClockPeriod
                )
            )