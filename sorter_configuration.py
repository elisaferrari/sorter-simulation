from configparser import ConfigParser
from pathlib import Path
import os, sys

# Configuration class, reads the configuration file and puts everything into a dictionary
class SorterConfiguration :
    def __init__(self,filename):

        # Initialize configuration parser
        config = ConfigParser()
        
        # Reading config file
        if not Path(filename).is_file():
            mess = 'Configuration file "{}" does not exist.'.format(filename)
            raise FileNotFoundError(mess)
        config.read(filename)

        # Building dictionary
        self.ConfigurationDictionary = {
            'global': 
            {
                'target_event_number': config.getint('global', 'target_event_number'),
                'detector_number': config.getint('global', 'detector_number'),
                'detector_event_rate': config.getfloat('global', 'detector_event_rate'),
                'channel_activation_probability': config.getfloat('global', 'channel_activation_probability'),
                'max_stages': config.getint('global', 'max_stages'),
                'max_threshold': config.getint('global', 'max_threshold')
            },
            'concentrator': 
            {
                'clock_frequency': config.getfloat('concentrator', 'clock_frequency'),
                'fifo_size': config.getint('concentrator', 'fifo_size')
            },
            'detector': 
            {
                'clock_frequency': config.getfloat('detector', 'clock_frequency'),
                'fifo_size': config.getint('detector', 'fifo_size'),
                'channels': config.getint('detector', 'channels'),
                'dead_time': config.getfloat('detector', 'dead_time')
            },
            'sorter_stage': 
            {
                'clock_frequency': config.getfloat('sorter_stage', 'clock_frequency'),
                'fifo_size': config.getint('sorter_stage', 'fifo_size'),
                'efficiency': config.getint('sorter_stage', 'efficiency')
            } 
        }
        
        # Checking dictionary
        self.CheckDictionary()


    # Function to check the dictionary for inconsistencies. For now, it checks only if the values are positive
    def CheckDictionary(self):
        for key in self.ConfigurationDictionary.values():
            for val in key.values():
                if (val <= 0):
                    raise ValueError('Bad configuration file: all numbers must be > 0')
        
        if self.ConfigurationDictionary['global']['channel_activation_probability'] >= 1:
            raise ValueError('Channel activation probability must be < 1.')

        